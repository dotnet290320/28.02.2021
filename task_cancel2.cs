using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks280221
{
    class Program
    {
        static void Main(string[] args)
        {
            CancellationTokenSource source = new CancellationTokenSource();
            Task taskRoot = Task.Run(() =>
            {
                int i = 0;
                while (i < 10 && !source.IsCancellationRequested)
                {
                    Console.WriteLine(i);
                    Thread.Sleep(1000);
                    i++;
                }
                source.Token.ThrowIfCancellationRequested();
            }, source.Token)
                .ContinueWith
                  ((Task papa) =>
                  {
                      if (papa.IsCanceled)
                      {
                          Console.WriteLine($"papa.IsCanceled = {papa.IsCanceled}");
                          Console.WriteLine($"papa.IsFaulted = {papa.IsFaulted}");
                      }
                  })
                  .ContinueWith
                  ((Task papa) =>
                  {
                      Console.WriteLine($"papa2.IsCanceled = {papa.IsCanceled}");
                      Console.WriteLine($"papa2.IsFaulted = {papa.IsFaulted}");
                  });
            ;//, TaskContinuationOptions.OnlyOnCanceled);
            //t1.Wait();

            Console.WriteLine("Press [Enter] to stop ...");
            Console.ReadLine();
            // exception
            //t1.Dispose(); 
            //m_finish = true;
            Console.WriteLine("Cancelling....");
            source.Cancel(); // ==> source.IsCancellationRequested = true

            Console.WriteLine("Press [Enter] to stop ...");
            Console.ReadLine();
            Console.WriteLine($"taskRoot.IsFaulted  {taskRoot.IsFaulted}");
            Console.WriteLine($"taskRoot.IsCanceled {taskRoot.IsCanceled}");
            Console.WriteLine($"taskRoot.IsCompleted {taskRoot.IsCompleted}");
            Console.WriteLine("Finish ...");
        }
    }
}
