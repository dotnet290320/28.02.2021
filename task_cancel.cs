using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks280221
{
    class Program
    {
        static ManualResetEvent are = new ManualResetEvent(false);

        static bool m_finish = false;
        static CancellationTokenSource source = new CancellationTokenSource();

        static void Printnumbers()
        {
            int i = 0;
            //while (i < 100 && !m_finish)
            while (i < 100 && !source.IsCancellationRequested)
            {
                Console.WriteLine(i);
                Thread.Sleep(1000);
                i++;
            }

            if (source.IsCancellationRequested)
            {
                throw new OperationCanceledException();
            }
        }

        static void Main(string[] args)
        {

            


          Task t1 = Task.Run(Printnumbers);
            //t1.Wait();

            Console.WriteLine("Press [Enter] to stop ...");
            Console.ReadLine();
            // exception
            //t1.Dispose(); 
            //m_finish = true;
            source.Cancel(); // ==> source.IsCancellationRequested = true

            Console.ReadLine();
            Console.WriteLine($"t1.IsFaulted  {t1.IsFaulted}");
            Console.WriteLine($"t1.IsCanceled {t1.IsCanceled}");
            Console.WriteLine($"t1.IsCompleted {t1.IsCompleted}");
            Console.WriteLine("Finish ...");
        }
    }
}
