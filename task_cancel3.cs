using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks280221
{
    class Program
    {
        static void Main(string[] args)
        {
            CancellationTokenSource source = new CancellationTokenSource();

            Task taskRoot = Task.Run(() =>
            {
                int a = 1;
                int b = 0;
                int c = a / b;
            }, source.Token)
                .ContinueWith((Task papa) =>
                {
                    // papa.Exception
                    int a = 1;
                    int b = 0;
                    int c = a / b;
                  int i = 0;
                    while (i < 10 && !source.IsCancellationRequested)
                    {
                        Console.WriteLine(i);
                        Thread.Sleep(1000);
                        i++;
                    }
                    source.Token.ThrowIfCancellationRequested();
                });
            //, TaskContinuationOptions.OnlyOnCanceled);
            //t1.Wait();

            Console.WriteLine("Press [Enter] to stop ...");
            Console.ReadLine();
            // exception
            //t1.Dispose(); 
            //m_finish = true;
            Console.WriteLine("Cancelling....");
            source.Cancel(); // ==> source.IsCancellationRequested = true

            try
            {
                taskRoot.Wait();
            }
            catch(AggregateException ex)
            {
                Console.WriteLine();
                int counter = 1;
                foreach(var ex_inner in ex.InnerExceptions)
                {
                    Console.WriteLine($"{counter++} {ex_inner.ToString()}");
                }
            }

            Console.WriteLine("Press [Enter] to stop ...");
            Console.ReadLine();
            Console.WriteLine($"taskRoot.IsFaulted  {taskRoot.IsFaulted}");
            Console.WriteLine($"taskRoot.IsCanceled {taskRoot.IsCanceled}");
            Console.WriteLine($"taskRoot.IsCompleted {taskRoot.IsCompleted}");
            Console.WriteLine("Finish ...");
        }
    }
}
