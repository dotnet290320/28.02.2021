using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks280221
{
    class Program
    {
        static void Main(string[] args)
        {
            CancellationTokenSource source = new CancellationTokenSource();

            try
            {
                Task.WaitAll(
                    new List<Task>()
                    {
                        Task.Run(() => {throw new ArgumentException("1"); }),
                        Task.Run(() => {throw new ArgumentException("2"); }),
                        Task.Run(() => {throw new ArgumentException("3"); })
                    }.ToArray()
                    );
            }
            catch (AggregateException ex)
            {
                Console.WriteLine();
                int counter = 1;
                foreach (var ex_inner in ex.InnerExceptions)
                {
                    Console.WriteLine($"{counter++} {ex_inner.ToString()}");
                }
            }
            Task t1 = null;
            t1 = Task.Run(() => { throw new ArgumentException("1"); });
            try
            {
                t1.Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine(t1.Exception.ToString()); // aggregate with inner ArgumentException
            }


            Thread.Sleep(3000);
        }
    }
}
