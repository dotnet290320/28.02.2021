using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks280221
{
    class Program
    {
        static ManualResetEvent are = new ManualResetEvent(false);

        static void Work(object i)
        {
            Console.WriteLine($"{i} Thread started...");

            Console.WriteLine($"{i} Waiting for my task ...");

            are.WaitOne();

            // pulling work from queue ...

            Console.WriteLine($"{i} Running my task ...");

            Console.WriteLine($"{i} Goodbye");
        }

        static void Main(string[] args)
        {
            int max = 1000;
            for (int i = 0; i < max; i++)
            {
                Thread t = new Thread(Work);
                t.Start(i);
            }

            Console.WriteLine("Press enter to release all threads to work eagerly! ");
            Console.ReadLine();
            are.Set();

        }
    }
}
